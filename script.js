let btnChangeTheme = document.getElementById('btn')

function changeTheme() {
    let theme = document.getElementById("theme");
    if(theme.getAttribute('href') == './styles/dark-theme.css'){
        localStorage.setItem('theme', 'light');
        theme.href = './styles/light-theme.css';        
    }else {
        localStorage.setItem('theme', 'dark');
        theme.href = './styles/dark-theme.css'        
    }
    return false;
}

btnChangeTheme.addEventListener('click', changeTheme);

if(localStorage.getItem('theme') === 'dark'){
    theme.href = './styles/dark-theme.css'
} else {
    theme.href = './styles/light-theme.css'
}
